import 'package:flutter/material.dart';

void main() {
  runApp(
    MaterialApp(
      debugShowCheckedModeBanner: false,
      title: "Hello Kenneth",
      home: Scaffold(
        appBar:
            AppBar(title: Text('My shapes'), backgroundColor: Colors.deepOrange),
        body: Center(
            child: Container(
                margin: EdgeInsets.all(10.0),
                child: Column(
                    children: [HelloTriangle()]
                )
            )
        ),
      ),
    ),
  );
}

class HelloRectangle extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Container(
        color: Colors.deepOrange,
        height: 400.0,
        width: 300.0,
        child: Center(
          child: Text('Hello ! I am Kenneth!',
              style: TextStyle(fontSize: 35), textAlign: TextAlign.center),
        ),
      ),
    );
  }
}

class HelloCircle extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Center(
        child: Container(
      margin: EdgeInsets.all(100.0),
      decoration: BoxDecoration(
          color: Colors.orange,
          shape: BoxShape.circle
      ),
    ));
  }
}

class HelloTriangle extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Center(
      child:
      Container(
        child: CustomPaint(
            size: Size(200, 200),
            painter: DrawTriangle()
        ),
      ),
    );
  }
}

class DrawTriangle extends CustomPainter {

  Paint _paint;

  DrawTriangle() {
    _paint = Paint()
      ..color = Colors.lightGreen
      ..style = PaintingStyle.fill;
    print(_paint.color);
  }

  @override
  void paint(Canvas canvas, Size size) {
    var path = Path();
    path.moveTo(size.width/2, 0);
    path.lineTo(0, size.height);
    path.lineTo(size.height, size.width);
    path.close();
    canvas.drawPath(path, _paint);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return false;
  }
}